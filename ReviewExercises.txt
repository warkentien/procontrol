#########################################################################
# Use this file to answer Review Exercises from the Big Java textbook
#########################################################################

R1.5 Benefits of using Java over machine code

1. Portability: Once it is written and compiled, it can be run anywhere that supports the Java platform.
2. Safety: Java's safety features make it possible to run Java programs in a browser without fear
that it could attack your computer. Java programs will also send error reports for issues that could compromise security
3. Since Java is memory-managed, runtime errors are greatly reduced, compared with unmanaged code

R2.3 Translating math expressions to Java.  See R2.4 for inspiration.

double s = sO + vO*t + (1/2)*g*Math.pow(t,2)
double G = 4*Math.pow(Math.PI,2)*Math.pow(a,3) / (Math.pow(p,2) * (m1 + m2))
double FV = PV * Math.pow((1 + INT/100), YRS) 
double c = Math.sqrt(Math.pow(a,2) + Math.pow(b,2) ? (2*a*b*Math.cos(gamma)))

R2.6 Order of precedence, and integer division.

a. 8.7
b. 1.0
c. 17.5
d. 17.5
e. 17.0
f. 18.0

R2.10 Formatting. Hint: DecimalFormat object.

It is returning the long decimal number because the double data type is a floating point number and unable to return
an exact representation of a number.
If you import java.text.DecimalFormat you can use DecimalFormat like this

        DecimalFormat myFormatter = new DecimalFormat("#,###.##");
        String output = myFormatter.format(change);
        System.out.println(output);

to print out 0.07

R2.11 Types expressed in code

A non-decimal number is considered an int in Java so 2 is an int
Decimal numbers are considered as doubles in Java so 2.0 is a double
A value in single quotes is a char, so '2' is a char
A value in double quotes a a string, so "2" is a string
For the same reason, "2.0" is also a string

R2.15 Math library and the modulus (%) operator

//get number from user and store in double variable called number
//take log10 of number and store in double called power
//use pow() to raise 10 to the calculated power variable, store this in double called base
//divide number variable by base variable and store in a double called first
//print out starting digit by casting first variable to int
//print out ending digit by taking number variable modulo 10

R3.18 Pseudocode

//store grade in a double called grade
//if grade >= 90
    //print A
//else if grade >= 80
    //print B
//else if grade >= 70
    //print C
//else if grade >= 60
    //print D
//else
    //print F

R4.9 Types of loops

Java supports while, for and do-while loops

while loop: you should use this loop if you want the block of code to be executed repeatedly until some condition is met
for loop: you should use this loop if you know how many times you want the code block to be executed
do-while loop: with this loop, the terminating condition is mentioned at the end of the code block, so you gurantee the block
will be executed at least once. It's different from the while loop because the terminating condition is at the beginning of the while
loop

R4.12 Pseudocode

//print heading of the table, which looks like "Celsius | Fahrenheit"
//print a dividing line that looks like "--------+-----------"
//set a double variable called cels to 0
//start a while loop that checks if cels is <= 100
//use the F to C formula "fahr = cels * 1.8 + 32" to set double variable fahr
//print cels + " | " + fahr, casting cels and fahr to ints
//increment cels by 10

R4.15 for/while

Rewrite the following for loop into a while loop.

int s = 0;
int i = 1;
while (i <= 10)
{
    s = s + i;
    i += 1;
}