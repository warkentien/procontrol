import java.util.Scanner;

/**
 * Created by Seth W. on 10/4/2015.
 */

public class P2_17 {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.print("Enter the first military time from 0000 to 2359 or \"quit\" to exit: ");
        int start = scan.nextInt();

        System.out.print("Enter the second military time from 0000 to 2359 or \"quit\" to exit: ");
        int end = scan.nextInt();

        int hours = 0;
        int min = 0;

        if (end > start) {
            hours = (end - start) / 100;
            min = (end - start) % 100;
        }
        else{
            hours = (start - end) / 100;
            min = (start - end) % 100;
        }

        System.out.println(hours + " hours " + min + " minutes" );

    }

}
