import java.util.Scanner;

/**
 * Created by sethf_000 on 10/4/2015.
 */
public class P4_18 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.print("Enter a number: ");
        int number = scan.nextInt();

        for (int numToCheck = 1; numToCheck <= number; numToCheck++) {
            int count = 0;

            for (int modulo = numToCheck; modulo >= 1; modulo--) {
                if (numToCheck%modulo == 0){
                    count+=1;
                }
            }

            if (count == 2){
                System.out.println(numToCheck);
            }

        }

    }
}
