import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by sethf_000 on 10/4/2015.
 */
public class P3_16 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.print("Enter three strings: ");
        String string = scan.nextLine();

        String[] strings = string.split(" ");

        Arrays.sort(strings);

        System.out.println(strings[0]);
        System.out.println(strings[1]);
        System.out.println(strings[2]);

    }
}
