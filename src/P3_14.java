import java.util.Scanner;

/**
 * Created by sethf_000 on 10/4/2015.
 */
public class P3_14 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.print("Enter the card notation: ");
        String cN = scan.nextLine();
        cN = cN.toUpperCase();

        String value = "";
        String suit = "";
        char first = 'a';
        char second= 'a';
        boolean flag = false;

        if (cN.length() == 2){
            first = cN.charAt(0);
            second= cN.charAt(1);
        }
        else{
            second= cN.charAt(2);
            flag = true;
        }

        if (first == 'A'){
            value = "Ace";
            flag = true;
        }
        else if (first == 'J'){
            value = "Jack";
            flag = true;
        }
        else if (first == 'Q'){
            value = "Queen";
            flag = true;
        }
        else if (first == 'K'){
            value = "King";
            flag = true;
        }


        if (second == 'D'){
            suit = "Diamonds";
        }
        else if (second == 'H'){
            suit = "Hearts";
        }
        else if (second == 'S'){
            suit = "Spades";
        }
        else if (second == 'C'){
            suit = "Clubs";
        }


        if (!flag){
            System.out.print("" + cN.charAt(0) + " of " + suit);
        }

        else{
            if (cN.length() == 2){
                System.out.println(value + " of " + suit);
            }
            else{
                System.out.print("" + cN.charAt(0) + cN.charAt(1));
                System.out.print(" of ");
                System.out.print(suit);
            }
        }




        System.out.println();

    }
}
