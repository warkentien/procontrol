import java.util.Scanner;

/**
 * Created by sethf_000 on 10/4/2015.
 */
public class P4_11 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.print("Enter a word: ");
        String word = scan.nextLine();
        word = word.toLowerCase();
        int count = 0;
        boolean syllable = false;

        for (int i = 0; i < word.length(); i++) {
            char letter = word.charAt(i);
            if (letter == 'a' || (letter == 'e' && i != word.length()-1) || letter == 'i' || letter == 'o' || letter == 'u'
                    || letter == 'y') {
               if (!syllable){
                   count += 1;
                   syllable = true;
               }
            }
            else{
                syllable = false;
            }

        }

        if (count == 0){
            count = 1;
        }

        System.out.print("The number of syllables is: " + count);
    }
}
