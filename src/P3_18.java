import java.util.Scanner;

/**
 * Created by sethf_000 on 10/4/2015.
 */
public class P3_18 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.print("Enter the month: ");
        String month = scan.nextLine();
        month = month.toLowerCase();
        System.out.print("Enter the day of the month: ");
        int day = scan.nextInt();

        int monthNo = 0;
        String season = "";

        String typeOfDay;
        switch (month) {
            case "january":
                monthNo = 1;
                break;
            case "february":
                monthNo = 2;
                break;
            case "march":
                monthNo = 3;
                break;
            case "april":
                monthNo = 4;
                break;
            case "may":
                monthNo = 5;
                break;
            case "june":
                monthNo = 6;
                break;
            case "july":
                monthNo = 7;
                break;
            case "august":
                monthNo = 8;
                break;
            case "september":
                monthNo = 9;
                break;
            case "october":
                monthNo = 10;
                break;
            case "november":
                monthNo = 11;
                break;
            case "december":
                monthNo = 12;
                break;
            default:
                throw new IllegalArgumentException("Invalid month: " + month);
        }

        if (monthNo == 1 || monthNo == 2 || monthNo == 3){
            season = "Winter";
        }

        else if (monthNo == 4 || monthNo == 5 || monthNo == 6){
            season = "Spring";
        }

        else if (monthNo == 7 || monthNo == 8 || monthNo == 9){
            season = "Summer";
        }
        else {
            season = "Fall";
        }

        if (monthNo%3 == 0 && day >= 21){
            if (season.equals("Winter")){
                season = "Spring";
            }
            else if (season.equals("Spring")){
                season = "Summer";
            }
            else if (season.equals("Summer")){
                season = "Fall";
            }
            else {
                season = "Winter";
            }
        }

        System.out.println("The season is: " + season);


    }
}
