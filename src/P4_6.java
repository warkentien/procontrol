import java.util.Scanner;

/**
 * Created by sethf_000 on 10/4/2015.
 */
public class P4_6 {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        System.out.print("Enter as many numbers as you like. Enter a non-number to exit the loop: ");
        int number = 0;
        int min = 0;

        boolean first = true;

        while(scan.hasNextInt()){

            int value = scan.nextInt();
            if (first) {
                min = value;
                first = false;
            }
            else if (value < min) {
                min = value;
            }
        }
        System.out.println("The lowest number entered was: " + min);
    }
}
