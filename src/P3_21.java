import java.util.Scanner;

/**
 * Created by sethf_000 on 10/4/2015.
 */
public class P3_21 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.print("Enter your income: ");
        double income = scan.nextDouble();
        double incomeTax;

        if (income <= 50000){
            incomeTax = (1*income)/100;
        }
        else if (income > 50000 && income <= 75000){
            incomeTax = (2*income)/100;
        }
        else if (income > 75000 && income <= 100000){
            incomeTax = (3*income)/100;
        }
        else if (income > 100000 && income <= 250000){
            incomeTax = (4*income)/100;
        }
        else if (income > 250000 && income <= 500000){
            incomeTax = (5*income)/100;
        }
        else {
            incomeTax = (6*income)/100;
        }

        System.out.println("Your income tax is: $" + incomeTax);


    }
}
