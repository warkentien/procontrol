/**
 * Created by Adam on 9/28/2015.
 */
public class P1_10 {

    public static void main(String[] args) {

        System.out.println("         __^__^__");
        System.out.println("        {        }");
        System.out.println("|___|   {  O  O  }    |___|    (Hi Earthlings!)");
        System.out.println(" | |    {________}     | |");
        System.out.println(" | |       |  |        | |");
        System.out.println(" --------          -------");
        System.out.println("         |      |");
        System.out.println("         |      |");
        System.out.println("         |      |");
        System.out.println("         |      |");
        System.out.println("         |      |");
        System.out.println("         ````````");
        System.out.println("      |     |  |     |");
        System.out.println("      |     |  |     |");
        System.out.println("      |     |  |     |");
        System.out.println("      |-----|  |-----|");


    }
}
