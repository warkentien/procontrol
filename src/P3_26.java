import java.util.Scanner;

/**
 * Created by sethf_000 on 10/4/2015.
 */
public class P3_26 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.print("Enter a number less than 4000: ");
        int number = scan.nextInt();

        if (number > 3999){
            System.out.print("Invalid input");
        }
        else{
            String numeral = "";
            int remainder = number%10;

            if (remainder > 0) {
                switch (remainder) {
                    case 1:
                        numeral = "I" + numeral;
                        break;
                    case 2:
                        numeral = "II" + numeral;
                        break;
                    case 3:
                        numeral = "III" + numeral;
                        break;
                    case 4:
                        numeral = "IV" + numeral;
                        break;
                    case 5:
                        numeral = "V" + numeral;
                        break;
                    case 6:
                        numeral = "VI" + numeral;
                        break;
                    case 7:
                        numeral = "VII" + numeral;
                        break;
                    case 8:
                        numeral = "VIII" + numeral;
                        break;
                    case 9:
                        numeral = "IX" + numeral;
                        break;
                }

            }

            number = number/10;
            remainder = number%10;

            if (remainder > 0) {
                switch (remainder) {
                    case 1:
                        numeral = "X" + numeral;
                        break;
                    case 2:
                        numeral = "XX" + numeral;
                        break;
                    case 3:
                        numeral = "XXX" + numeral;
                        break;
                    case 4:
                        numeral = "XL" + numeral;
                        break;
                    case 5:
                        numeral = "L" + numeral;
                        break;
                    case 6:
                        numeral = "LX" + numeral;
                        break;
                    case 7:
                        numeral = "LXX" + numeral;
                        break;
                    case 8:
                        numeral = "LXXX" + numeral;
                        break;
                    case 9:
                        numeral = "XC" + numeral;
                        break;
                }

            }

            number = number/10;
            remainder = number%10;

            if (remainder > 0) {
                switch (remainder) {
                    case 1:
                        numeral = "C" + numeral;
                        break;
                    case 2:
                        numeral = "CC" + numeral;
                        break;
                    case 3:
                        numeral = "CCC" + numeral;
                        break;
                    case 4:
                        numeral = "CD" + numeral;
                        break;
                    case 5:
                        numeral = "D" + numeral;
                        break;
                    case 6:
                        numeral = "DC" + numeral;
                        break;
                    case 7:
                        numeral = "DCC" + numeral;
                        break;
                    case 8:
                        numeral = "DCCC" + numeral;
                        break;
                    case 9:
                        numeral = "CM" + numeral;
                        break;
                }

            }

            number = number/10;
            remainder = number%10;

            if (remainder > 0) {
                switch (remainder) {
                    case 1:
                        numeral = "M" + numeral;
                        break;
                    case 2:
                        numeral = "MM" + numeral;
                        break;
                    case 3:
                        numeral = "MMM" + numeral;
                        break;
                }

            }

            System.out.println("Number converted to Roman numeral: " + numeral);

        }

    }
}
